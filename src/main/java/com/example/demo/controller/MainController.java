package com.example.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Carga;
import com.example.demo.service.IMainService;

@RestController
public class MainController {
	
	@Autowired IMainService ms;
	
	@PostMapping("/carga")
	public Carga carga(@RequestBody Carga carga) {
				System.out.println(carga.getId());
				System.out.println(carga.getNombre());
				//ms.save(carga.getId(), carga.getNombre());
				ms.save(carga);
				
		return carga;
		
	}
	
	@PostMapping("/descarga")
	public String descarga(@RequestBody Carga carga) {
				System.out.println(carga.getId());
				
				
		return null;
		
	}
	

}
