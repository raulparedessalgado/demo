package com.example.demo.repo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Carga;
import com.example.demo.repo.IMainRepo;
import com.example.demo.sql.Query;

@Repository
public class MainRepo implements IMainRepo {

	@Autowired JdbcTemplate jdbcTemplate;
	
	@Override
	public void save(Carga carga) {
		// TODO Auto-generated method stub
		try {
			int resp = jdbcTemplate.update(Query.SQL_SAVE, new Object[] {
											carga.getId(), carga.getNombre()
											});
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
